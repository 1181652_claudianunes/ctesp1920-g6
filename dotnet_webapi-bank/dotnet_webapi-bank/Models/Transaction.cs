﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnet_webapi_bank.Models
{
    public class Transaction
    {
        [Key]
        public int IdTransaction { get; set; }
        public int IdAccount { get; set; }
        [ForeignKey("IdAccount")]
        public DateTime Day { get; set; }
        public decimal ValueTrans { get; set; }
        public decimal Balance { get; set; }
        public int IdTypeTransaction { get; set; }
        [ForeignKey("IdTypeTransaction")]

        public virtual BankAccount BankAccount { get; set; }
        public virtual TransactionType TransactionType { get; set; }

    }
}
