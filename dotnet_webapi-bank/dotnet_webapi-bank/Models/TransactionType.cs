﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_webapi_bank.Models
{
    public class TransactionType
    {
        [Key]
        public int IdTypeTransaction { get; set; }
        public string TypeTrans { get; set; }
    }
}
