﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_webapi_bank.Models
{
    public class BankAccount
    {
        [Key]
        public int IdAccount { get; set; }
        public int IdClient { get; set; }
        [ForeignKey("IdClient")]
        public string AccountNum { get; set; }
        public string Iban { get; set; }
        public decimal Balance { get; set; }

        public virtual Client Client { get; set; }
        public virtual List<Transaction> Transaction { get; set; }

    }
}
