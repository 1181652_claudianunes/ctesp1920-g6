﻿using System;
using dotnet_webapi_bank.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace dotnet_webapi_bank.Infrastructure
{
    public class DbApiContext : DbContext
    {
        public DbApiContext(DbContextOptions<DbApiContext> options) : base(options)
        {
            //if (BankAccount.Count() == 0)
            //{
            //    AddTestData(this);
            //}
        }
        public DbSet<BankAccount> BankAccount { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<TransactionType> TransactionType { get; set; }
        private static void AddTestData(DbApiContext context)
        {
            List<BankAccount> BankAccount = new List<BankAccount>();
            List<Client> Client = new List<Client>();
            List<Transaction> Transaction = new List<Transaction>();
            List<TransactionType> TransactionType = new List<TransactionType>();
            BankAccount.Add(new BankAccount
            {
                IdAccount = 1,
                IdClient = 1,
                AccountNum = "123456",
                Iban = "102131510245",
                Balance = 5
            });

            Client.Add(new Client
            {
                IdClient = 1,
                Name = "Zé",
                Address = "estrada de paralelo",
                Contact = "916116438"
            });
            Transaction.Add(new Transaction
            {
                IdTransaction = 1,
                IdAccount = 1,
                Day = DateTime.UtcNow.AddDays(-5),
                IdTypeTransaction = 1,
                Balance = 3000,
                ValueTrans = 4
            });
            TransactionType.Add(new TransactionType
            {
                IdTypeTransaction = 1,
                TypeTrans = "coiso"
            });
            TransactionType.Add(new TransactionType
            {
                IdTypeTransaction = 2,
                TypeTrans = "coisinho"
            });
            context.BankAccount.AddRange(BankAccount);
            context.Client.AddRange(Client);
            context.Transaction.AddRange(Transaction);
            context.TransactionType.AddRange(TransactionType);
            context.SaveChanges();
        }
}
}
