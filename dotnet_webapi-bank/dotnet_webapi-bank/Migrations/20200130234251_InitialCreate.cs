﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace dotnet_webapi_bank.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    IdClient = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Contact = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.IdClient);
                });

            migrationBuilder.CreateTable(
                name: "TransactionType",
                columns: table => new
                {
                    IdTypeTransaction = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeTrans = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionType", x => x.IdTypeTransaction);
                });

            migrationBuilder.CreateTable(
                name: "BankAccount",
                columns: table => new
                {
                    IdAccount = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdClient = table.Column<int>(nullable: false),
                    AccountNum = table.Column<string>(nullable: true),
                    Iban = table.Column<string>(nullable: true),
                    Balance = table.Column<decimal>(nullable: false),
                    ClientIdClient = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankAccount", x => x.IdAccount);
                    table.ForeignKey(
                        name: "FK_BankAccount_Client_ClientIdClient",
                        column: x => x.ClientIdClient,
                        principalTable: "Client",
                        principalColumn: "IdClient",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Transaction",
                columns: table => new
                {
                    IdTransaction = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdAccount = table.Column<int>(nullable: false),
                    Day = table.Column<DateTime>(nullable: false),
                    ValueTrans = table.Column<decimal>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false),
                    IdTypeTransaction = table.Column<int>(nullable: false),
                    TransactionTypeIdTypeTransaction = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transaction", x => x.IdTransaction);
                    table.ForeignKey(
                        name: "FK_Transaction_BankAccount_IdTypeTransaction",
                        column: x => x.IdTypeTransaction,
                        principalTable: "BankAccount",
                        principalColumn: "IdAccount",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transaction_TransactionType_TransactionTypeIdTypeTransaction",
                        column: x => x.TransactionTypeIdTypeTransaction,
                        principalTable: "TransactionType",
                        principalColumn: "IdTypeTransaction",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankAccount_ClientIdClient",
                table: "BankAccount",
                column: "ClientIdClient");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_IdTypeTransaction",
                table: "Transaction",
                column: "IdTypeTransaction");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_TransactionTypeIdTypeTransaction",
                table: "Transaction",
                column: "TransactionTypeIdTypeTransaction");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transaction");

            migrationBuilder.DropTable(
                name: "BankAccount");

            migrationBuilder.DropTable(
                name: "TransactionType");

            migrationBuilder.DropTable(
                name: "Client");
        }
    }
}
