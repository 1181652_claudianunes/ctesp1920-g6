﻿using System;
using System.Linq;
using System.Threading.Tasks;

using dotnet_webapi_bank.Infrastructure;

using dotnet_webapi_bank.Models;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using dotnet_webapi_bank.Validation;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace dotnet_webapi_bank.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    [ApiController]
    public class BankAccountController : Controller
    {
        private readonly DbApiContext _context;
        public BankAccountController(DbApiContext context)
        {
            _context = context;
        }


        // Transação
        [HttpGet("{id}/Transaction")]
        public IActionResult Get(int? id)
        {
            if (BanckAccountValidations.NotNull(id))
            {
                return BadRequest();
            }
            var response = _context.Transaction.Where(x => x.IdAccount == id && x.Day.Date >= DateTime.UtcNow.AddDays(-30).Date)
                    .Select(
                    (c => new
                    {
                        TotalCredits = -c.IdTypeTransaction.CompareTo(1),
                        TotalDebits = -c.IdTypeTransaction.CompareTo(2),
                        Date = c.Day.ToString("dd/MM/yyyy"),
                        Balance = c.Balance 
                    }));

            if (BanckAccountValidations.NotNullResponse(response))
            {
                return NotFound();
            }

            return Ok(response);
        }


        [HttpGet("{id}")]
        public IActionResult GetId(int? id)
        {
            if (BanckAccountValidations.NotNull(id))
            {
                return BadRequest();
            }
            var response = _context.BankAccount.Where(x => x.IdAccount == id)
                   .Select(
                   (account => new
                   {
                       ClientName = account.Client.Name,
                       ClientAddress = account.Client.Address,
                       ClientContact = account.Client.Contact,
                       Balance = account.Balance
                   }));

            if (BanckAccountValidations.NotNullResponse(response))
            {
                return NotFound();
            }

            return Ok(response);
        }


        [HttpPost]
        public async Task<ActionResult<Transaction>> Add(Transaction transaction)
        {
            if (BanckAccountValidations.NotNullTransaction(transaction))
            {
                return BadRequest();
            }

            try
            {
                transaction.Day = DateTime.UtcNow;
                if (BanckAccountValidations.Idtype1Transaction(transaction))
                {
                    transaction.Balance = transaction.BankAccount.Balance - transaction.ValueTrans;
                }
                else if (BanckAccountValidations.Idtype2Transaction(transaction))
                {
                   transaction.Balance = transaction.BankAccount.Balance + transaction.ValueTrans;
                }
                else
                {
                    return BadRequest();
                }

                await _context.Transaction.AddAsync(transaction);
                await _context.SaveChangesAsync();
                BankAccount originalAccount= await _context.BankAccount.SingleOrDefaultAsync(x => x.IdAccount == transaction.BankAccount.IdAccount);
                _context.Entry(originalAccount).CurrentValues.SetValues(transaction.BankAccount);
                await _context.SaveChangesAsync();

                //if depostito e depositos

                //invoice.Id = await _context.Invoices.MaxAsync(x => x.Id) + 1;



                var response = new
                {
                    transaction.BankAccount.AccountNum,
                    transaction.Day,
                    transaction.ValueTrans,
                    transaction.IdTypeTransaction
                };
                return Ok(response);

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }



    

    }
}
