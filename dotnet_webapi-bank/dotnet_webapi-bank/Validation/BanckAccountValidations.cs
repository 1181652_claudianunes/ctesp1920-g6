﻿using System;
using System.Linq;

namespace dotnet_webapi_bank.Validation
{
    public class BanckAccountValidations
    {
        public static Boolean NotNull(int? val)
        {
            if (val.Equals(null))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static Boolean NotNullResponse(IQueryable<object> val)
        {
            if (val.Equals(null))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static Boolean NotNullTransaction(dotnet_webapi_bank.Models.Transaction val)
        {
            if (val.Equals(null) || val.ValueTrans <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static Boolean Idtype1Transaction(dotnet_webapi_bank.Models.Transaction val)
        {
            if (val.IdTypeTransaction == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static Boolean Idtype2Transaction(dotnet_webapi_bank.Models.Transaction val)
        {
            if (val.IdTypeTransaction == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
