﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using dotnet_webapi_bank.Models;

namespace dotnet_webapi_bank.Validation.Tests
{
    [TestClass()]
    public class BanckAccountValidationsTests
    {
        [TestMethod()]
        public void NotNullTest()
        {
            Assert.IsFalse(BanckAccountValidations.NotNull(1));
        }

        [TestMethod()]
        public void NotNullTransactionTest()
        {
            dotnet_webapi_bank.Models.Transaction val = (new Transaction
            {
                IdTransaction = 1,
                IdAccount = 1,
                Day = DateTime.UtcNow.AddDays(-5),
                IdTypeTransaction = 1,
                Balance = 3000,
                ValueTrans = 4
            });
            Assert.IsFalse(BanckAccountValidations.NotNullTransaction(val));
        }

        [TestMethod()]
        public void Idtype1TransactionTest()
        {
            dotnet_webapi_bank.Models.Transaction val = (new Transaction
            {
                IdTransaction = 1,
                IdAccount = 1,
                Day = DateTime.UtcNow.AddDays(-5),
                IdTypeTransaction = 1,
                Balance = 3000,
                ValueTrans = 4
            });
            Assert.IsTrue(BanckAccountValidations.Idtype1Transaction(val));
        }

        [TestMethod()]
        public void Idtype2TransactionTest()
        {
            dotnet_webapi_bank.Models.Transaction val = (new Transaction
            {
                IdTransaction = 1,
                IdAccount = 1,
                Day = DateTime.UtcNow.AddDays(-5),
                IdTypeTransaction = 2,
                Balance = 3000,
                ValueTrans = 4
            });
            Assert.IsTrue(BanckAccountValidations.Idtype2Transaction(val));
        }
        [TestMethod()]
        public void NotNullTestFail()
        {
            Assert.IsTrue(BanckAccountValidations.NotNull(null));
        }

    }
}